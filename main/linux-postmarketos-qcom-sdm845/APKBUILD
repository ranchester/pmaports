# Maintainer: Caleb Connolly <caleb@connolly.tech>
# Co-Maintainer: Joel Selvaraj <jo@jsfamily.in>
# Stable Linux kernel with patches for SDM845 devices
# Kernel config based on: arch/arm64/configs/defconfig and sdm845.config

_flavor="postmarketos-qcom-sdm845"
pkgname=linux-$_flavor
pkgver=5.12.15
pkgrel=0
pkgdesc="Mainline Kernel fork for SDM845 devices"
arch="aarch64"
_carch="arm64"
url="https://gitlab.com/sdm845-mainline/linux"
license="GPL-2.0-only"
options="!strip !check !tracedeps
	pmb:cross-native
	pmb:kconfigcheck-nftables
	pmb:kconfigcheck-containers
	pmb:kconfigcheck-zram
	pmb:kconfigcheck-anbox"
makedepends="bison findutils flex installkernel openssl-dev perl"

_repo="linux"
_config="config-$_flavor.$arch"
_commit="8cd3168c87c0c26e481305a0491d651a1ef3b7b2"

# Source
source="
	$_repo-$_commit.tar.gz::https://gitlab.com/sdm845-mainline/$_repo/-/archive/$_commit/$_repo-$_commit.tar.gz
	$_config
"
builddir="$srcdir/$_repo-$_commit"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$arch" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-$_flavor"
}

package() {
	mkdir -p "$pkgdir"/boot
	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot/ \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_DTBS_PATH="$pkgdir"/usr/share/dtb
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir"/usr/share/kernel/$_flavor/kernel.release
}

sha512sums="
05c45e78fd56fb77a2055f1bd895bf188a27386719af050c41d7eeccd83923776eb07fd372d06bda26bc3bacfd48072bea7a990d2d45a9b1093f18b054a5a9dc  linux-8cd3168c87c0c26e481305a0491d651a1ef3b7b2.tar.gz
154c0a56185fc24e29d1e41e9b03614b6fcd60ab10e27a381533b0fbdab5ad3a2902f483b4edecc561136334549a985d5173e5033244f46daee49b783c3434f7  config-postmarketos-qcom-sdm845.aarch64
"
